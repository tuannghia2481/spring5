package aptech.fpt.spring.model;

import aptech.fpt.spring.entity.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Integer> {
}
